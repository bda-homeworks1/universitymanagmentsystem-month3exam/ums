package dbconfig.services;




import dbconfig.connect.DBConnection;

import java.sql.*;
import java.util.ArrayList;
import java.util.Scanner;

import static dbconfig.services.AdminServices.showAdminMenu;
import static dbconfig.services.StudentServices.showStudentMenu;
import static dbconfig.services.TeacherServices.showMyGroups;
import static dbconfig.services.TeacherServices.showTeacherMenu;

public class DBServices {

    public static Connection con = DBConnection.getConnection();
    static private    ArrayList<Object> datas = new ArrayList<>();

    public static PreparedStatement preparedStatement = null;

    public static int usersID ;
    public static int studentID ;
    public static int teacherID ;
    public static int groupID ;
    public static int journalID ;
    public static int groupDataID ;
    public static int journalDataID ;
    public static int enteredPersonalId ;
    public static int enteredRoleId ;
    public static int enteredGroupId ;
    public static boolean isEntered = false ;

    public static Scanner scanner = new Scanner(System.in);

    public static void mainLoginOfUsers(){
    enteredPersonalId = 0;
    enteredRoleId = 0;
    enteredGroupId = 0;

    System.out.println("username:");
    String username = scanner.next();
    System.out.println("password");
    String password = scanner.next();



    try {
        String query = "Select * from users";
        preparedStatement = con.prepareStatement(query);
        ResultSet result = preparedStatement.executeQuery(query);


        while (result.next()) {
            for (int i = 4; i <= 4; i++) {
                if(username.equals(result.getString(i))){
                    if (password.equals(result.getString(5))){
                        enteredPersonalId = result.getInt(7);
                        enteredRoleId = result.getInt(6);
                        isEntered = true;
                        break;
                    }
                }
            }
        }




    } catch (SQLException e) {
        throw new RuntimeException(e);
    }

    if (enteredRoleId==1){
        showAdminMenu();
    }
    else if (enteredRoleId==2){
        showMyGroups();
        showTeacherMenu();
    }
    else if (enteredRoleId == 3) {
        try {
            String query = "select * from students";
            preparedStatement = con.prepareStatement(query);
            ResultSet result = preparedStatement.executeQuery(query);

            while (result.next()) {

                for (int i = 5; i <= 5; i++) {
                    if(result.getInt(1) == enteredPersonalId){
                        enteredGroupId = result.getInt(7);
                    }
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        showStudentMenu();
    }

}
}
