package dbconfig.services;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;
import static dbconfig.services.DBServices.*;


public class StudentServices {
    static Scanner scanner = new Scanner(System.in);

    public static void showStudentMenu(){
        System.out.println("Student Menu");
//        System.out.println("1-show my group");
        System.out.println("2-show my teachers");
        System.out.println("3-show my lessons");
        System.out.println("4-exit");
        System.out.println("-------------");

        String command = scanner.next();
        switch (command) {
//            case "1" -> showMyGroups();
            case "2" ->  showMyGroupTeachers();
            case "3" -> showMyLessons();
            case "4" -> mainLoginOfUsers();
        }



    }

    public static void showMyGroupTeachers(){
        try {
            String query = "select name, surname\n" +
                    "from teachers\n" +
                    "join ums.group_data gd on teachers.id = gd.teacherId and groupId = " + enteredGroupId;
            preparedStatement = con.prepareStatement(query);
            ResultSet result = preparedStatement.executeQuery();

            while (result.next()) {
                String data = "";
                for (int i = 1; i <= 2; i++) {
                    data+=result.getString(i) + "  ";
                }
                System.out.println(data);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    public static void showMyLessons(){
        try {
            String query = "select subject\n" +
                    "from teachers\n" +
                    "join ums.group_data gd on teachers.id = gd.teacherId and groupId = " + enteredGroupId;
            preparedStatement = con.prepareStatement(query);
            ResultSet result = preparedStatement.executeQuery();

            while (result.next()) {
                String data = "";
                for (int i = 1; i <= 1; i++) {
                    data+=result.getString(i) + "  ";
                }
                System.out.println(data);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
//
//    public static void showMyGroups(){
//        System.out.println("group number " + students.get(students.indexOf(users.get(enterId))).groupNumber);
//        showStudentMenu();
//    }
//
//    public static void showMyTeachers(){
//        System.out.println("My Teachers");
//        for (model.Teacher teacher : teachers) {
//            if (teacher.groupNumber == students.get(students.indexOf(users.get(enterId))).groupNumber) {
//                System.out.println(teacher.getUsername());
//            }
//        }
//        showStudentMenu();
//    }
//
//    public static void showMySciences(){
//        System.out.println("My Sciences");
//        for (model.Teacher teacher : teachers) {
//            if (teacher.groupNumber == students.get(students.indexOf(users.get(enterId))).groupNumber) {
//                System.out.println(teacher.profession);
//            }
//        }
//        showStudentMenu();
//    }





}
