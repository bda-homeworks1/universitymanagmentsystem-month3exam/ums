package dbconfig.services;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import static dbconfig.services.DBServices.*;

public class AdminServices {
    static Scanner scanner = new Scanner(System.in);

    public static void showAdminMenu(){
        System.out.println("Admin Menu");
        System.out.println("1-create teacher");
        System.out.println("2-create student");
        System.out.println("3-create group");
        System.out.println("4-show teachers");
        System.out.println("5-show students");
        System.out.println("6-show groups");
        System.out.println("7-add teacher to group");
        System.out.println("8-exit");
        System.out.println("--------------------");
        System.out.println("emr secin:");

        selectCommand();
    }

    public static void selectCommand(){
        System.out.println("select command");
        String command = scanner.next();
        switch (command) {
            case "1" -> {
                insertTeacher();
                showAdminMenu();
            }
            case "2" -> {
                insertStudent();
                showAdminMenu();
            }
            case "3" -> {
                insertGroup();
                showAdminMenu();
            }
            case "4" -> {
                showAllTeachers();
                showAdminMenu();
            }
            case "5" -> {
                showAllStudents();
                showAdminMenu();
            }
            case "6" -> {
                showGroups();
                showAdminMenu();
            }
            case "7" -> {
                addTeacherToGroup();
                showAdminMenu();
            }
            case "8" -> mainLoginOfUsers();
        }
    }
    public static void insertStudent(){


        try {
            String query = "Select * from students";
            preparedStatement = con.prepareStatement(query);
            ResultSet result = preparedStatement.executeQuery(query);


            while (result.next()) {

                for (int i = 1; i <= 1; i++) {
                    studentID=result.getInt(i);
                }
            }
            studentID+=1;


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


        System.out.println("Student add");
        System.out.println("Ad");
        String name = scanner.next();
        System.out.println("Soyad");
        String surname = scanner.next();
        System.out.println("username");
        String username = scanner.next();
        System.out.println("password");
        String password = scanner.next();
        System.out.println("Group Id");
        int groupId = scanner.nextInt();

        try {
            String query = "Select * from users";
            preparedStatement = con.prepareStatement(query);
            ResultSet result = preparedStatement.executeQuery(query);


            while (result.next()) {

                for (int i = 1; i <= 1; i++) {
                    usersID=result.getInt(i);
                }
            }
            usersID+=1;


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        try{


            String query2 = "INSERT INTO users(id,name,surname,username, password, roleId, personal_id ) " +
                    "VALUES(?,?,?,?,?,?,?)";
            preparedStatement= con.prepareStatement(query2);
            preparedStatement.setInt(1,usersID);
            preparedStatement.setString(2,name);
            preparedStatement.setString(3,surname);
            preparedStatement.setString(4,username);
            preparedStatement.setString(5,password);
            preparedStatement.setInt(6,3);
            preparedStatement.setInt(7,studentID);



            preparedStatement.execute();
            System.out.println("User Data inserted succesfully !");


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


        try{


            String query2 = "INSERT INTO students(id,name,surname,username, password, groupId ) " +
                    "VALUES(?,?,?,?,?,?)";
            preparedStatement= con.prepareStatement(query2);
            preparedStatement.setInt(1,studentID);
            preparedStatement.setString(2,name);
            preparedStatement.setString(3,surname);
            preparedStatement.setString(4,username);
            preparedStatement.setString(5,password);
            preparedStatement.setInt(6,groupId);



            preparedStatement.execute();
            System.out.println("Student Data inserted succesfully !");


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }




    }
    public static void insertTeacher(){
        try {
            String query = "Select * from teachers";
            preparedStatement = con.prepareStatement(query);
            ResultSet result = preparedStatement.executeQuery(query);


            while (result.next()) {

                for (int i = 1; i <= 1; i++) {
                    teacherID=result.getInt(i);
                }
            }
            teacherID+=1;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        try{
            System.out.println("Teacher add");
            System.out.println("Ad");
            String name = scanner.next();
            System.out.println("Soyad");
            String surname = scanner.next();
            System.out.println("username");
            String username = scanner.next();
            System.out.println("password");
            String password = scanner.next();
            System.out.println("fenn");
            String subject = scanner.next();

            try {
                String query = "Select * from users";
                preparedStatement = con.prepareStatement(query);
                ResultSet result = preparedStatement.executeQuery(query);


                while (result.next()) {

                    for (int i = 1; i <= 1; i++) {
                        usersID=result.getInt(i);
                    }
                }
                usersID+=1;


            } catch (SQLException e) {
                throw new RuntimeException(e);
            }

            try{


                String query2 = "INSERT INTO users(id,name,surname,username, password, roleId, personal_id ) " +
                        "VALUES(?,?,?,?,?,?,?)";
                preparedStatement= con.prepareStatement(query2);
                preparedStatement.setInt(1,usersID);
                preparedStatement.setString(2,name);
                preparedStatement.setString(3,surname);
                preparedStatement.setString(4,username);
                preparedStatement.setString(5,password);
                preparedStatement.setInt(6,2);
                preparedStatement.setInt(7,teacherID);



                preparedStatement.execute();
                System.out.println("User Data inserted succesfully !");


            } catch (SQLException e) {
                throw new RuntimeException(e);
            }



            String query = "INSERT INTO teachers(id,name,surname,username,password,subject)" +
                    "VALUES(?,?,?,?,?,?)";
            preparedStatement = con.prepareStatement(query);
            preparedStatement.setInt(1,teacherID);
            preparedStatement.setString(2,name);
            preparedStatement.setString(3,surname);
            preparedStatement.setString(4,username);
            preparedStatement.setString(5,password);
            preparedStatement.setString(6,subject);
            preparedStatement.execute();


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }



    }
    public static void insertGroup(){
        try {
            String query = "Select * from `groups`";
            preparedStatement = con.prepareStatement(query);
            ResultSet result = preparedStatement.executeQuery(query);


            while (result.next()) {

                for (int i = 1; i <= 1; i++) {
                    groupID=result.getInt(i);
                }
            }
            groupID+=1;


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


        System.out.println("Group add");
        System.out.println("Qrup adi");
        String name = scanner.next();

        try{


            String query2 = "INSERT INTO `groups`(id,groupName) " +
                    "VALUES(?,?)";
            preparedStatement= con.prepareStatement(query2);
            preparedStatement.setInt(1,groupID);
            preparedStatement.setString(2,name);
            preparedStatement.execute();

            insertJournal();
            System.out.println("Data inserted succesfully !");


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


    }
    public static void insertJournal(){
        try {
            String query = "Select * from journals";
            preparedStatement = con.prepareStatement(query);
            ResultSet result = preparedStatement.executeQuery(query);


            while (result.next()) {

                for (int i = 1; i <= 1; i++) {
                    journalID=result.getInt(i);
                }
            }
            journalID+=1;


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }



        try{


            String query2 = "INSERT INTO journals(id,groupId ) " +
                    "VALUES(?,?)";
            preparedStatement= con.prepareStatement(query2);
            preparedStatement.setInt(1,journalID);
            preparedStatement.setInt(2,groupID);




            preparedStatement.execute();
            System.out.println("Journal inserted succesfully !");


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }




    }
    public static void addTeacherToGroup(){
        try {
            String query = "Select * from group_data";
            preparedStatement = con.prepareStatement(query);
            ResultSet result = preparedStatement.executeQuery(query);


            while (result.next()) {

                for (int i = 1; i <= 1; i++) {
                    groupDataID=result.getInt(i);
                }
            }
            groupDataID+=1;


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        showGroups();
        System.out.println("Qrup id-si secin");
        int selectedGroupId = scanner.nextInt();

        showAllTeachers();
        System.out.println("Muellim id-si secin");
        int selectedTeacherId = scanner.nextInt();




        try{


            String query2 = "INSERT INTO group_data(id,groupId,teacherId ) " +
                    "VALUES(?,?,?)";
            preparedStatement= con.prepareStatement(query2);
            preparedStatement.setInt(1,groupDataID);
            preparedStatement.setInt(2,selectedGroupId);
            preparedStatement.setInt(3,selectedTeacherId);

            preparedStatement.execute();
            System.out.println("Teacher adding succesfully !");


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }




    }
    public static void showGroups(){
        try {
            String query = "Select * from `groups`;";
            preparedStatement = con.prepareStatement(query);
            ResultSet result = preparedStatement.executeQuery();

            while (result.next()) {
                String data = "";
                for (int i = 1; i <= 2; i++) {
                    data+=result.getString(i) + " | ";
                }
                System.out.println(data);

            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    public static void showAllStudents(){
        try {
            String query = "Select * from students;";
            preparedStatement = con.prepareStatement(query);
            ResultSet result = preparedStatement.executeQuery();

//            System.out.println("id-     name  -  surname  - username - password -  roleId - groupId");
            while (result.next()) {
                String data = "";
                for (int i = 1; i <= 7; i++) {
                    data+=result.getString(i) + "   |   ";
                }
                System.out.println(data);
//                datas.add(data);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    public static void showAllTeachers(){
        try {
            String query = "Select * from teachers;";
            preparedStatement = con.prepareStatement(query);
            ResultSet result = preparedStatement.executeQuery();

            while (result.next()) {
                String data = "";
                for (int i = 1; i <= 7; i++) {
                    data+=result.getString(i) + " | ";
                }
                System.out.println(data);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }


}
