package dbconfig.services;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;
import static dbconfig.services.DBServices.*;


public class TeacherServices {
    static Scanner scanner = new Scanner(System.in);

    public static void showTeacherMenu(){
        System.out.println("Teacher Menu");
        System.out.println("1-show entered group");
        System.out.println("2-show my subject");
        System.out.println("3-show journal - not ready");
        System.out.println("4-show entered group");
        System.out.println("5-show children in this group");
        System.out.println("6-add data to Journal");
        System.out.println("7-exit");
        System.out.println("--------------------");
        System.out.println("emr secin:");
        String command = scanner.next();

        switch (command){
            case "1" -> {
                showMyGroups();
                showTeacherMenu();
            }
            case "2" -> {
                showMySubject();
                showTeacherMenu();
            }
            case "3"-> {
                showThisGroupJournal();
                showTeacherMenu();
            }
            case "4"-> {
                showEnteredGroup();
                showTeacherMenu();
            }
            case "5"-> {
                showThisGroupChildren();
                showTeacherMenu();
            }
            case "6"-> {
                insertJournalData();
                showTeacherMenu();
            }
            case "7" -> mainLoginOfUsers();

        }

    }

    public static void showMyGroups(){
        try {
            String query = "select *\n" +
                    "from group_data\n" +
                    "join ums.`groups` g on g.id = group_data.groupId and teacherId =" + enteredPersonalId;
            preparedStatement = con.prepareStatement(query);
            ResultSet result = preparedStatement.executeQuery(query);


            while (result.next()) {

                for (int i = 5; i <= 5; i++) {
                    System.out.println(result.getString(i) + " - qrupun id -si " + result.getString(1));
                }
            }
            System.out.println("Qrupunuzu id ile secin:");
            enteredGroupId = scanner.nextInt();
            System.out.println("Qrupa giris etdiniz");


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static void showEnteredGroup(){
        try{
            String query = "select *\n" +
                    "from `groups`\n" +
                    "where id = "+ enteredGroupId;
            preparedStatement = con.prepareStatement(query);
            ResultSet result = preparedStatement.executeQuery(query);

            while(result.next()){
                for (int i = 1; i <= 1; i++) {
                    System.out.println("giris etdiyim qrup - " + result.getString(i+1));
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static void showThisGroupChildren(){
        try{
            String query = "select name, surname\n" +
                    "from students\n" +
                    "where groupId = " + enteredGroupId;
            preparedStatement = con.prepareStatement(query);
            ResultSet result = preparedStatement.executeQuery(query);

            while (result.next()){
                String stud = "";
                for (int i = 1; i <= 2; i++) {
                    stud += result.getString(i) + " ";
                }
                System.out.println(stud);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static void showMySubject(){
        try {
            String query = "select subject\n" +
                    "from teachers\n" +
                    "where id =" + enteredPersonalId;
            preparedStatement = con.prepareStatement(query);
            ResultSet result = preparedStatement.executeQuery(query);


            while (result.next()) {

                for (int i = 1; i <= 1; i++) {
                    System.out.println(result.getString(i) + " - my subject" );
                }
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static void showThisGroupJournal(){
        try{
            String query = "select *\n" +
                    "from journal_data\n" +
                    "where groupId =" + enteredGroupId + " and teacherId = " + enteredPersonalId;
            preparedStatement = con.prepareStatement(query);
            ResultSet result = preparedStatement.executeQuery(query);

            while (result.next()){
                String data = "";
                for (int i = 1; i <= 7; i++) {
                    data += result.getString(i) + " ";
                }
                System.out.println(data);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static void insertJournalData(){
        System.out.println("Journal Data add");
        showThisGroupChildren();


        System.out.println("Sagirdi id-sine gore secin");
        int studentId = scanner.nextInt();
        System.out.println("Tarix");
        String date = scanner.next();
        System.out.println("Davamiyyet ( q / + ):");
        String attendance = scanner.next();


        try {
            String query = "Select * from journal_data";
            preparedStatement = con.prepareStatement(query);
            ResultSet result = preparedStatement.executeQuery(query);


            while (result.next()) {

                for (int i = 1; i <= 1; i++) {
                    journalDataID=result.getInt(i);
                }
            }
            journalDataID+=1;


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        try{


            String query2 = "INSERT INTO journal_data(id, groupId, journalId, teacherId, studentId, date, attendance ) " +
                    "VALUES(?,?,?,?,?,?,?)";
            preparedStatement= con.prepareStatement(query2);
            preparedStatement.setInt(1,journalDataID);
            preparedStatement.setInt(2,enteredGroupId);
            preparedStatement.setInt(3,enteredGroupId);
            preparedStatement.setInt(4,enteredPersonalId);
            preparedStatement.setInt(5,studentId);
            preparedStatement.setString(6,date);
            preparedStatement.setString(7,attendance);



            preparedStatement.execute();
            System.out.println("Journal Data inserted succesfully !");


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }


}
