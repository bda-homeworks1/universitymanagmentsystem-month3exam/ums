package dbconfig.connect;



import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
    private static  Connection con = null;

    public static Connection getConnection()  {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        try {
            con = DriverManager.getConnection("jdbc:mysql://localhost:3310/ums", "root", "root");
            System.out.println("Connection to database has succesfully !");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return con;
    }

    public static void closeConnection(){
        try {
            con.close();
            System.out.println("DB Connection closed succesfully !");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
