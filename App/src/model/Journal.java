package model;

import java.util.ArrayList;

public class Journal {
    public int groupNumber;
    public ArrayList<JournalData> journals = new ArrayList<>();

    public Journal(int groupNumber,ArrayList<JournalData> journals) {
        this.journals = journals;
        this.groupNumber=groupNumber;
    }

    @Override
    public String toString() {
        return "Journal{" +
                "groupNumber=" + groupNumber +
                ", journals=" + journals.toString() +
                '}';
    }
}
