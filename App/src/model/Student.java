package model;

import java.util.ArrayList;

public class Student extends User{
    public int groupId;

    public ArrayList<JournalData> journals = new ArrayList<>();

    public Student(int id, String name, String surname, String username, String password, int roleId, int groupId) {
        super(id, name, surname, username, password, roleId);
        this.groupId = groupId;
    }
}
