package model;

import java.time.LocalDate;

public class JournalData {

    public int groupNumber;
    public String student;
    public String teacherUsername;
    public String science;
    public LocalDate dateTime;
    public String attendance;

    public JournalData(int groupNumber, String student, String science, String teacherUsername) {
        this.groupNumber = groupNumber;
        this.teacherUsername = teacherUsername;
        this.student = student;
        this.science = science;
    }

    @Override
    public String toString() {
        return "JournalData{" +
                "groupNumber=" + groupNumber +
                ", student='" + student + '\'' +
                ", teacherUsername='" + teacherUsername + '\'' +
                ", science='" + science + '\'' +
                ", dateTime=" + dateTime +
                ", attendance='" + attendance + '\'' +
                '}';
    }
}
