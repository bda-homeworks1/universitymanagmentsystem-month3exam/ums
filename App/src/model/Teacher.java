package model;


public class Teacher extends User {

    public String subject;


    public Teacher(int id, String name, String surname, String username, String password, int roleId, String subject) {
        super(id, name, surname, username, password, roleId);
        this.subject = subject;
    }
}
