package model;

import java.util.ArrayList;

public class Group {
    public int groupNumber;
   public ArrayList<Teacher> teachers = new ArrayList<>();
   public ArrayList<Student> students = new ArrayList<>();
   public ArrayList<JournalData> groupJournal = new ArrayList<>();

    public Group(int groupNumber, ArrayList<Teacher> teachers, ArrayList<Student> students, ArrayList<JournalData> journals) {
        this.groupNumber = groupNumber;
        this.teachers = teachers;
        this.students = students;
        this.groupJournal = journals;
    }

    @Override
    public String toString() {
        return "Group{" +
                "groupNumber=" + groupNumber +
                ", teachers=" + teachers +
                ", students=" + students +
                '}';
    }
}
